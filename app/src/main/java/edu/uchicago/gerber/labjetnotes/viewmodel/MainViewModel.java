package edu.uchicago.gerber.labjetnotes.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;



import java.util.List;

import edu.uchicago.gerber.labjetnotes.model.AppRepository;
import edu.uchicago.gerber.labjetnotes.model.NoteEntity;
import edu.uchicago.gerber.labjetnotes.utilities.SampleData;

public class MainViewModel extends AndroidViewModel {

    public LiveData<List<NoteEntity>> mNotes;
    private AppRepository mRepository;

    public MainViewModel(@NonNull Application application) {
        super(application);

        mRepository = AppRepository.getInstance(application.getApplicationContext());
        mNotes = mRepository.mNotes;
    }

    public void addSampleData() {
        mRepository.addSampleData();
    }
    public void deleteAllNotes() {
        mRepository.deleteAllNotes();
    }
}